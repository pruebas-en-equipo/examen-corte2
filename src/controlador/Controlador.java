package controlador;

import vista.*;
import modelo.*;

import javax.swing.JOptionPane;
import javax.swing.JFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

public class Controlador implements ActionListener{
    private dlgVenta vistaV;
    private dlgRegistros vistaR;
    private dbVentas db;
    private Ventas venta;
    private int inventarioR = 50, inventarioP = 500;
    
    public Controlador(dlgVenta vistaV, dlgRegistros vistaR, Ventas venta, dbVentas db){
        this.vistaV = vistaV;
        this.vistaR = vistaR;
        this.venta = venta;
        this.db = db;
        // Escuchar eventos
        this.vistaV.btnNuevo.addActionListener(this);
        this.vistaV.btnBuscar.addActionListener(this);
        this.vistaV.btnRealizar.addActionListener(this);
        this.vistaV.btnGuardar.addActionListener(this);
        this.vistaV.btnMostrar.addActionListener(this);
        this.vistaR.btnMostrar.addActionListener(this);
        this.vistaR.btnRegresar.addActionListener(this);
    }
    
    private Boolean validarInventario(int inv){
        switch(inv){
            case 1:
                return this.inventarioR < this.venta.getCantidad();
            case 2:
                return this.inventarioP < this.venta.getCantidad();
        }
        return false;
    }
    
    public void inicializarVistaV(String title){
        this.vistaV.setTitle(title);
        this.vistaV.setLocationRelativeTo(null);
        this.vistaV.setVisible(true);
    }
    
    public void inicializarVistaR(String title){
        this.vistaR.setTitle(title);
        this.vistaR.setLocationRelativeTo(null);
        this.vistaR.setVisible(true);
    }
    
    public static void main(String[] args){
       dlgVenta vistaV = new dlgVenta(new JFrame(), true);
       dlgRegistros vistaR = new dlgRegistros(new JFrame(), true);
       Ventas venta = new Ventas();
       dbVentas db = new dbVentas();
       Controlador controlador = new Controlador(vistaV, vistaR, venta, db);
       controlador.inicializarVistaV("Ventas");
    }
    
    private void cargarDatos(){
        ArrayList<Ventas> lista = new ArrayList<>();
        String columnas[] = {"Código", "Cantidad", "Tipo de Gasolina", "Precio por litro", "Total a pagar"};
        Object[][] filas;
        int numRegistros;
        try{
            numRegistros = this.db.numRegistros();
            filas = new Object[numRegistros][columnas.length];
            lista = this.db.listar();
            for(int i = 0; i < numRegistros; i++){
                filas[i][0] = lista.get(i).getCodigo();
                filas[i][1] = lista.get(i).getCantidad();
                filas[i][2] = lista.get(i).getTipo();
                filas[i][3] = lista.get(i).getPrecio();
                filas[i][4] = lista.get(i).getTotal();
            }
            DefaultTableModel modelo = new DefaultTableModel(filas, columnas);
            this.vistaR.jtbRegistros.setModel(modelo);
        }
        catch(Exception ex){
            System.err.print("Surgio una excepcion al cargar datos: " + ex.getMessage());
        }
    }
    
    private void limpiar(){
        this.vistaV.txtCodigo.setText("");
        this.vistaV.txtCantidad.setText("");
        this.vistaV.cmbTipos.setSelectedIndex(0);
        this.vistaV.lblCantidad.setText("0 Ltrs.");
        this.vistaV.lblPrecio.setText("$ 0");
        this.vistaV.lblTotal.setText("$ 0");
        this.vistaV.txtCodigo.requestFocusInWindow();
    }
    
    private Boolean isVacio(){
        return this.vistaV.txtCodigo.getText().equals("") ||
               this.vistaV.txtCantidad.getText().equals("");
    }
    
    private Boolean isNotValido(){
        return Double.parseDouble(this.vistaV.txtCantidad.getText()) <= 0 ||
               this.vistaV.cmbTipos.getSelectedIndex() == 0;
    }
    
    private void habilitarComponentes(Boolean bool){
        this.vistaV.txtCodigo.setEnabled(bool);
        this.vistaV.txtCantidad.setEnabled(bool);
        this.vistaV.cmbTipos.setEnabled(bool);
        this.vistaV.btnRealizar.setEnabled(bool);
        this.vistaV.btnBuscar.setEnabled(bool);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == this.vistaV.btnNuevo){
            this.limpiar();
            this.habilitarComponentes(Boolean.TRUE);
        }
        if(e.getSource() == this.vistaV.btnBuscar){
            if(this.vistaV.txtCodigo.getText().equals("")){
                JOptionPane.showMessageDialog(this.vistaV, "No puedes buscar un código vacío", "Ventas", JOptionPane.WARNING_MESSAGE);
                return;
            }
            try{
                this.venta = (Ventas)this.db.buscar(this.vistaV.txtCodigo.getText());
                if(this.venta == null){
                    JOptionPane.showMessageDialog(this.vistaV, "No se encontró el código especificado", "Ventas", JOptionPane.WARNING_MESSAGE);
                    return;
                }
                this.vistaV.txtCantidad.setText(String.valueOf(this.venta.getCantidad()));
                this.vistaV.cmbTipos.setSelectedIndex(this.venta.getTipo());
                this.vistaV.lblCantidad.setText("$ " + String.valueOf(this.venta.getCantidad()));
                this.vistaV.lblPrecio.setText("$ " + String.valueOf(this.venta.getPrecio()));
                this.vistaV.lblTotal.setText("$ " + String.valueOf(this.venta.getTotal()));
            }
            catch(Exception ex){
                JOptionPane.showMessageDialog(this.vistaV, "Ocurrió un error: " + ex.getMessage(), "Ventas", JOptionPane.ERROR_MESSAGE);
            }
        }
        if(e.getSource() == this.vistaV.btnRealizar){
            if(this.isVacio()){
                JOptionPane.showMessageDialog(this.vistaV, "Asegurate de tener campos vacíos antes de hacer cualquier operación", "Ventas", JOptionPane.WARNING_MESSAGE);
                return;
            }
            if(this.isNotValido()){
                JOptionPane.showMessageDialog(this.vistaV, "Cantidad o Tipo de gasolina no válidos", "Ventas", JOptionPane.WARNING_MESSAGE);
                return;
            }
            this.venta = new Ventas();
            this.venta.setCodigo(this.vistaV.txtCodigo.getText());
            this.venta.setCantidad(Integer.parseInt(this.vistaV.txtCantidad.getText()));
            this.venta.setTipo(this.vistaV.cmbTipos.getSelectedIndex());
            switch(this.vistaV.cmbTipos.getSelectedItem().toString()){
                case "Gasolina Regular":
                    this.venta.setPrecio(20.50);
                    break;
                case "Gasolina Premium":
                    this.venta.setPrecio(24.50);
                    break;
            }
            this.venta.setTotal(this.venta.calcularTotal());            
            this.vistaV.lblCantidad.setText(this.venta.getCantidad() + " Ltrs.");
            this.vistaV.lblPrecio.setText("$ " + this.venta.getPrecio());
            this.vistaV.lblTotal.setText("$ " + this.venta.getTotal());
            this.vistaV.btnGuardar.setEnabled(true);
        }
        if(e.getSource() == this.vistaV.btnGuardar){
            if(this.isVacio()){
                JOptionPane.showMessageDialog(this.vistaV, "Asegurate de tener campos vacíos antes de hacer cualquier operación", "Ventas", JOptionPane.WARNING_MESSAGE);
                return;
            }
            if(this.isNotValido()){
                JOptionPane.showMessageDialog(this.vistaV, "Cantidad o Tipo de gasolina no válidos", "Ventas", JOptionPane.WARNING_MESSAGE);
                return;
            }
            try{
                switch(this.vistaV.cmbTipos.getSelectedItem().toString()){
                    case "Gasolina Regular":
                        if(this.validarInventario(1)){
                            JOptionPane.showMessageDialog(this.vistaV, "No hay suficiente gasolina regular", "Ventas", JOptionPane.WARNING_MESSAGE);
                            return;
                        }
                        this.inventarioR -= this.venta.getCantidad();
                        break;
                    case "Gasolina Premium":
                        if(this.validarInventario(2)){
                            JOptionPane.showMessageDialog(this.vistaV, "No hay suficiente gasolina premium", "Ventas", JOptionPane.WARNING_MESSAGE);
                            return;
                        }
                        this.inventarioP -= this.venta.getCantidad();
                        break;
                }
                if(this.db.buscar(this.vistaV.txtCodigo.getText()) != null){
                    this.db.actualizar(this.venta);
                    JOptionPane.showMessageDialog(this.vistaR, "Se actualizó la venta con código " + this.vistaV.txtCodigo.getText() + " con éxito", "Ventas", JOptionPane.INFORMATION_MESSAGE);
                    return;
                }
                this.db.insertar(this.venta);
                JOptionPane.showMessageDialog(this.vistaR, "Se registró una nueva venta con éxito", "Ventas", JOptionPane.INFORMATION_MESSAGE);
            }
            catch(Exception ex){
                JOptionPane.showMessageDialog(this.vistaV, "Ocurrió un error: " + ex.getMessage(), "Ventas", JOptionPane.ERROR_MESSAGE);
            }
        }
        if(e.getSource() == this.vistaV.btnMostrar){
            this.inicializarVistaR("Registros");
        }
        
        if(e.getSource() == this.vistaR.btnRegresar){
            this.vistaR.setVisible(false);
        }
        if(e.getSource() == this.vistaR.btnMostrar){
            this.cargarDatos();
        }
    }
}
