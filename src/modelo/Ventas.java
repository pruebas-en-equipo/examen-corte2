package modelo;

public class Ventas {

    private String codigo;
    private double precio;
    private int cantidad;
    private int tipo;
    private double total;
    
    public Ventas(String codigo, double precio, int cantidad, int tipo, double total) {
        this.codigo = codigo;
        this.precio = precio;
        this.cantidad = cantidad;
        this.tipo = tipo;
        this.total = total;
    }

    public Ventas() {
        this.codigo = "";
        this.precio = 0;
        this.cantidad = 0;
        this.tipo = 0;
        this.total = 0;
    }
    
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
    
    public double calcularTotal(){
        return this.cantidad * this.precio;
    }
}

