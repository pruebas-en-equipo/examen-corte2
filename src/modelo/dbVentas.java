package modelo;
import java.sql.SQLException;
import java.util.ArrayList;

public class dbVentas extends dbManejador implements dbPersistencia {

    @Override
    public void insertar(Object objeto) throws Exception {
        Ventas venta = new Ventas();
        venta = (Ventas) objeto;
        String consulta = "Insert into ventas(codigo,tipo,precio,cantidad,total) values (?,?,?,?,?)";

        if (this.conectar()) {
            try {
                System.err.println("Se conecto correctamente");
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                // asignacion de valores a la consulta
                this.sqlConsulta.setString(1, venta.getCodigo());
                this.sqlConsulta.setInt(2, venta.getTipo());
                this.sqlConsulta.setDouble(3, venta.getPrecio());
                this.sqlConsulta.setInt(4, venta.getCantidad());
                this.sqlConsulta.setDouble(5, venta.getTotal());
                this.sqlConsulta.executeUpdate();
                this.desconectar();
            } catch (SQLException e) {
                System.err.println("Surgio un error al agregar el nuevo usuario" + e.getMessage());
            }
        }
    }

    @Override
    public void actualizar(Object objeto) throws Exception {
        Ventas venta = new Ventas();
        venta = (Ventas) objeto;
        
        String consulta = "UPDATE ventas SET tipo = ?, precio = ?, cantidad = ?, total = ? WHERE codigo = ?";

        if (this.conectar()) {
            try {
                System.err.println("Se conecto correctamente");
                this.sqlConsulta = this.conexion.prepareStatement(consulta);

                // asignacion de valores a la consulta
                this.sqlConsulta.setInt(1, venta.getTipo());
                this.sqlConsulta.setDouble(2, venta.getPrecio());
                this.sqlConsulta.setInt(3, venta.getCantidad());
                this.sqlConsulta.setDouble(4, venta.getTotal());
                this.sqlConsulta.setString(5, venta.getCodigo());

                this.sqlConsulta.executeUpdate();
                this.desconectar();

            } catch (SQLException e) {
                System.err.println("Surgio un error al actualizar el producto" + e.getMessage());
            }
        }
    }
    
    @Override
    public ArrayList listar() throws Exception {
        ArrayList<Ventas> lista = new ArrayList<>();
        Ventas venta;
        if (this.conectar()) {
            String consulta = "Select * from ventas";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.registros = this.sqlConsulta.executeQuery();
            //sacar los registros en el arreglo
            while (this.registros.next()) {
                venta = new Ventas();
                venta.setCodigo(this.registros.getString("codigo"));
                venta.setTipo(this.registros.getInt("tipo"));
                venta.setPrecio(this.registros.getDouble("precio"));
                venta.setCantidad(this.registros.getInt("cantidad"));
                venta.setTotal(this.registros.getDouble("total"));
                lista.add(venta);
            }
        }
        this.desconectar();
        return lista;
    }

    @Override
    public Object buscar(String userName) throws Exception {
        Ventas venta = null;
        if (this.conectar()) {
            String consulta = "SELECT * FROM ventas WHERE codigo = ?;";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, userName);
            this.registros = this.sqlConsulta.executeQuery();
            if (this.registros.next()) {
                venta = new Ventas();
                venta.setCodigo(this.registros.getString("codigo"));
                venta.setTipo(this.registros.getInt("tipo"));
                venta.setPrecio(this.registros.getDouble("precio"));
                venta.setCantidad(this.registros.getInt("cantidad"));
                venta.setTotal(this.registros.getDouble("total"));
            }
            this.desconectar();
        }
        return venta;
    }

    @Override
    public int numRegistros() throws Exception {
        int numRegistros = 0;
        if (this.conectar()) {
            String consulta = "SELECT * FROM ventas;";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.registros = this.sqlConsulta.executeQuery();
            while (this.registros.next()) {
                numRegistros++;
            }
            this.desconectar();
        }
        return numRegistros;
    }

}
